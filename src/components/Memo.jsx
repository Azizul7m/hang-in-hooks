import React, { useCallback, useMemo, useState } from 'react'


function SortedList({ list, sortFunc }) {
  console.log("Out side of memo: " + list)

  const sortName = useMemo(() => {
    console.log("under use Memo: " + list)
    return [...list].sort(sortFunc)
  }, [list]);
  return (
    <p>Sort Name: {sortName.join(", ")}</p>
  )
}


const Memo = () => {
  const [names] = useState(['jhon', 'bob', 'ambine', 'mike', 'stack', 'Zake']);
  // const sortName = [...names].sort();
  const sortFunc = useCallback((a, b) => a.localeCompare(b) * -1, []);
  return (
    <div>
      <h6>Names: {names.join(", ")}</h6>
      <SortedList list={names} sortFunc={sortFunc} />
    </div>
  )
}

export default Memo
